import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home'
import Asset from '@/pages/asset'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/asset',
      name: 'Asset',
      component: Asset
    }
  ]
})
