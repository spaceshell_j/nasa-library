/* global fetch */

import './audio-box.less'

export default {
  name: 'audio-box',
  data () {
    return {
      loaded: false,
      tracks: []
    }
  },
  props: [
    'audio'
  ],
  watch: {
    audio (nxt) {
      nxt.map(aud => fetch(aud.audio)
        .then(resp => resp.json())
        .then(media => {
          this.tracks = this.tracks.concat({
            ...aud,
            media
          })
        })
      )

      this.loaded = true
    }
  },
  render (vc) {
    return vc('section', {
      attrs: {
        id: 'audio-box'
      }
    },
      this.tracks.map(track => vc('div', {
        class: 'audio-track'
      }, [
        vc('h3', {
          class: 'truncate'
        }, [track.description]),
        vc('audio', {
          attrs: {
            controls: ''
          }
        },
          track.media.map(source => {
            // const media = ['wav', 'm4a', 'mp3', 'ogg', 'flac']
            const ext = source.split('.').slice(-1)[0]

            return vc('source', {
              attrs: {
                src: source,
                type: `audio/${ext}`
              }
            })
          })
        )
      ]))
    )
  }
}
