/* global sessionStorage */

/*
*
*
* @author James Nicholls [j-n@hotmail.co.uk]
*/

export default function UrlQueries (base, queryList) {
  if (!base) throw new Error('Base URL Required')

  // Restructures arrays into and objects into an object array
  // NOTE Does not handle numbers as Object keys
  function queryStringObject (que) {
    if (typeof que === 'string') que = [].concat(que) // Typecheck String

    return Object.keys(que).map((key, idx) => {
      const keyNew = /^[0-9]+$/.test(key) // If key matches index e.g an Array flip KVP
        ? que[key]
        : key

      return { [keyNew]: (que[keyNew] || undefined) } // Returns KVP with value or undefined
    })
  }

  function getQueryObjectFromUrl (url) { // Decompiles URL string into object components
    const que = url.split('?')[1]

    return que.split('&').map(q => {
      const [key, val] = q.split('=')

      return { [decodeURIComponent(key)]: decodeURIComponent(val) } // URI encoding / Decoding
    })
  }

  return {
    queryParts: queryList // Initialise the return object
      ? queryStringObject(queryList) // With list, set internal object
      : getQueryObjectFromUrl(base), // Without list, deconstruct string
    base: base.split('?').shift(), // Recover Url domain and path
    getQueryUrl (queries = this.queryParts, address = this.base) { // Compiles the Queries object into a URL string
      return queries.reduce((str, q, idx) => {
        const [key, value] = Object.entries(q).pop()

        !idx ? str += '?' : str += '&' // Concatenates entries to string

        str += value
          ? `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
          : `${encodeURIComponent(key)}`

        return str
      }, address)
    },
    getLocalQueries () { // TODO Json parsing
      const address = sessionStorage.getItem('queries')

      this.queryParts = this.queryStringObject(address.queries)
    },
    setLocalQueries () {
      sessionStorage.setItem('queries', this.queryParts)
    },
    queriesAdd (queries) {
      const queriesObj = queryStringObject(queries)

      this.queriesRemove(queries) // Remove existing keys
      this.queryParts = this.queryParts.concat(queriesObj)

      return this
    },
    queriesRemove (queries) {
      queries = Array.isArray(queries)
        ? queries // Removes existing as input Array
        : Object.keys(queries) // Removes existing as input Object

      queries = [].concat(queries)
      if (!this.queryParts) throw new Error('No queries to remove')
      this.queryParts = this.queryParts.filter(que => {
        return !queries.includes(...Object.keys(que))
      })

      return this
    }
  }
}
