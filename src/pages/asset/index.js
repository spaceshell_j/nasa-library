/* global fetch */

export default {
  name: 'Asset',
  data () {
    return {
      imageAddress: this.imageLink
    }
  },
  props: {
    imageLink: {
      type: String,
      required: true
    }
  },
  methods: {
  },
  watch: {
    imageLink: (value) => {
      return fetch(value)
        .then(resp => resp.json())
        .then(json => {
          this.imageAddress = json[0]
        })
    }
  },
  created () {
  },
  render (v) {
    return v('img', {
      src: this.imageLink,
      alt: 'Main Image',
      height: '42',
      width: '42'
    })
  }
}
